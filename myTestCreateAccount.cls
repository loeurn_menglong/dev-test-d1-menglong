@isTest
Public class myTestCreateAccount {
    static testMethod void testAccountTrigger(){
        //create new account
		Account acc = new Account(Name='Hugh Crinion ltd');
    	insert acc;
        
    	//create contact for new account
        Contact[] contactsToCreate = new Contact[]{};
            for(integer i=0; i<2; i++){
        		Contact ct = new Contact(AccountId=acc.Id, 
                           				FirstName='Hugh', 
                           				LastName='Crinion', 
                           				Email='hcrinion@gmail.com');
        		contactsToCreate.add(ct);
        	}
        test.startTest();
        insert contactsToCreate;
    	test.stopTest();
    }
}